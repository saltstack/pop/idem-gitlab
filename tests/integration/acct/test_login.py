async def test_gather(hub, ctx):
    profiles = {
        "gitlab.login": {
            "default": {
                "endpoint_url": ctx.acct.endpoint_url,
                "username": "root",
                "password": "not_secure",
            }
        }
    }
    ret = await hub.acct.gitlab.login.gather(profiles)

    assert ret["default"]
