async def test_gather(hub, ctx):
    profiles = {
        "gitlab": {
            "default": {
                "endpoint_url": ctx.acct.endpoint_url,
                "token": "token",
                "namespace": 1,
            }
        }
    }
    ret = await hub.acct.gitlab.init.gather(profiles)

    assert ret["default"]["endpoint_url"] == ctx.acct.endpoint_url
    assert ret["default"]["headers"] == {"PRIVATE-TOKEN": "token"}
