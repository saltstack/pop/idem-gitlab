import asyncio
import pathlib
import tempfile
import uuid
from enum import Enum
from typing import Any
from unittest import mock

import pytest
import pytest_asyncio
import pytest_idem.runner
import yaml
from dict_tools.data import NamespaceDict


class TestFlag(Enum):
    """
    Enum for the __test fixture
    """

    TEST = "--test"
    RUN = "run"
    TEST_NO_CHANGE = "--test (no-change)"
    RUN_NO_CHANGE = "run (no-change)"

    def __bool__(self) -> bool:
        if "TEST" in self.name:
            return True
        return False


@pytest.fixture(
    scope="function",
    name="__test",
    params=[
        TestFlag.TEST,
        TestFlag.RUN,
        TestFlag.TEST_NO_CHANGE,
        TestFlag.RUN_NO_CHANGE,
    ],
    ids=[
        TestFlag.TEST.value,
        TestFlag.RUN.value,
        TestFlag.TEST_NO_CHANGE.value,
        TestFlag.RUN_NO_CHANGE.value,
    ],
)
def test_flag(ctx, request):
    """
    Functions that use the __test fixture will be run four times
    0. With the --test flag set, to test what happens before creating a resource
    1. Without the --test flag set, to create a resource
    2. With the --test flag set, to verify that no changes would be made
    3. Without the --test flag set, to verify that no changes are made
    """
    ctx.test = bool(request.param)
    yield request.param


@pytest.fixture(scope="module")
def resource_name(request) -> str:
    """
    Provide a unique and descriptive name for an arbitrary resource
    """
    module_name = pathlib.Path(request.module.__file__).stem
    return f"idem-gitlab-{module_name}-{uuid.uuid4()}".replace("-", "_")


class Resource(NamespaceDict):
    """
    A dictionary that will dump itself to a yaml file and cleans up after use
    """

    __fh__: tempfile.NamedTemporaryFile = None
    __ref__: str = None

    def __enter__(self):
        """
        Dump the contents of the Resource to a yaml file and return the path
        """
        assert self.__ref__, "resource.__ref__ needs to be set in the test module."

        fh = tempfile.NamedTemporaryFile(suffix=".sls", delete=False)
        self.__fh__ = pathlib.Path(fh.name)

        state = {
            self.get("name", "resource"): {
                f"{self.__ref__}.present": [{k: v} for k, v in self.items()]
            }
        }
        self.__fh__.write_text(yaml.safe_dump(state))
        return str(self.__fh__)

    def __exit__(self, *args):
        """
        Close the tempfile, which deletes it, and dereference it.
        """
        self.__fh__.unlink()
        self.__fh__ = None


@pytest.fixture(scope="module")
def resource(resource_name) -> Resource:
    """
    Create a dictionary-like object that will preserve a resource's state across test functions in the same module.
    This should be populated with attributes necessary for the resource to be created at the beginning of the test module.

    I.E.

    .. code-block:: python

        @pytest.fixture(scope="module", autouse=True)
        def resource_init(resource):
            resource.__ref__ = "gitlab.project.project"
            resource.path = f"idem-gitlab-test-project-{uuid.uuid4()}"

    After the present state runs for real the first time, update the "resource" fixture with the new state.

    I.E.

    .. code-block:: python

        @pytest.mark.dependency(name="present")
        def test_present(idem_cli, resource: str, __test: TestFlag):
            with resource as sls:
                ret = idem_cli("state", sls)

            if __test is TestFlag.RUN:
                resource.update(state_ret.new_state)

    This enables passing of "resource_id" and other attributes from the present state to the absent state.
    """
    r = Resource(name=resource_name)
    yield r


@pytest.fixture(scope="session", autouse=True)
def acct_subs() -> list[str]:
    """
    Needed by pytest-idem to determine which provider to load from acct for the tests.
    """
    return ["gitlab"]


@pytest.fixture(scope="session", autouse=True)
def acct_profile() -> str:
    """
    Needed by pytest-idem to determine which profile to load from acct for the tests.
    """
    return "test_development_idem_gitlab"


@pytest.fixture(scope="session")
def event_loop():
    """
    Patch the pytest-asyncio event loop to exist for the entire session
    """
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="module", name="hub")
def integration_hub(hub, event_loop):
    """
    Create a hub for the integration tests that uses the session event loop
    and patches hub.OPT to have generic defaults.
    """
    hub.pop.sub.add(dyne_name="idem")

    hub.pop.loop.CURRENT_LOOP = event_loop

    with mock.patch("sys.argv", ["idem"]):
        hub.pop.config.load(hub.idem.CONFIG_LOAD, "idem", parse_cli=False)

    yield hub


@pytest_asyncio.fixture(scope="module", name="ctx")
async def integration_ctx(
    hub, acct_subs: list[str], acct_profile: str
) -> dict[str, Any]:
    """
    Override pytest-idem's ctx fixture to use a default profile if no acct_file was provided.
    """
    ctx = NamespaceDict(
        run_name="test",
        test=False,
        tag="fake_|-test_|-tag",
        old_state={},
    )

    # Fallback to the credentials used for gitlab-ce on localhost
    acct_file = (
        hub.OPT.acct.acct_file
        or pathlib.Path(__file__).parent.parent.parent / "example" / "credentials.yaml"
    )

    # Add the profile to the account
    await hub.acct.init.unlock(acct_file, hub.OPT.acct.acct_key)
    ctx.acct = await hub.acct.init.gather(acct_subs, acct_profile)
    if not ctx.acct:
        raise Exception(
            f'Unable to load acct "{acct_profile}" from "{hub.OPT.acct.acct_file}"'
        )
    yield ctx


@pytest.fixture(scope="function", name="idem_cli")
def idem_cli_mod(ctx):
    """
    Patch the idem_cli fixture to implicitly add the "--test" flag as necessary
    based on the how the __test fixture modified ctx.acct.

    Use this to run the idem cli the same way it is run by users.

    .. code-block:: python

        def test_function(idem_cli):
            ret = idem_cli("exec", sls)
            assert ret["result"], ret["stderr"]
            exec_ret = ret["json"]

    Use this fixture with the resource fixture to run states.

    .. code-block:: python

        def test_function(resource, idem_cli):
            with resource as sls:
                ret = idem_cli("state", sls)
            assert ret["result"], ret["stderr"]
            state_ret = next(iter(ret["json"].values()))
            assert state_ret["result"], state_ret["comment"]
    """
    assert ctx.acct, "No acct profile found"

    def wrapper(*args, **kwargs):
        if "state" in args:
            args = (*args, "--reconciler=none", "--hard-fail")
            if ctx.test:
                args = (*args, "--test")
        kwargs["acct_data"] = {"profiles": {"gitlab": {"default": ctx.acct}}}
        return pytest_idem.runner.idem_cli(*args, **kwargs)

    return wrapper
