"""Tests for validating Project Commits"""
import copy

import pytest


@pytest.fixture(scope="module", autouse=True)
async def resource_init(hub, resource, gitlab_test_project, gitlab_test_commit):
    """
    Initialize the necessary attributes of the resource to create it
    """
    resource.__ref__ = "gitlab.project.commit"
    # Use the commit fixture to create, we don't manage commits with idem, only list and git
    resource.update(gitlab_test_commit)


def test_get(idem_cli, resource):
    """
    Verify that we can get the resource using the "idem exec" cli call.
    """
    ret = idem_cli(
        "exec",
        "gitlab.project.commit.get",
        resource.resource_id,
        f"project_id={resource.project_id}",
    )

    # Verify that the subprocess ran successfully
    assert ret["result"], ret["stderr"]
    assert ret["json"], "Output did not result in readable json"

    # Verify the output of the exec module
    get_ret = ret["json"]
    assert get_ret["result"], get_ret["comment"]
    resource_copy = copy.copy(resource)
    resource_copy.pop("name", None)
    assert get_ret["ret"] == resource_copy


def test_list(idem_cli, resource):
    """
    Verify that we can list the resource using the "idem exec" cli call.
    """
    ret = idem_cli("exec", "gitlab.project.commit.list")

    # Verify that the subprocess ran successfully
    assert ret["result"], ret["stderr"]
    assert ret["json"], "Output did not result in readable json"

    # Verify that the resource created by the present state exists in the list
    list_ret = ret["json"]
    assert list_ret["result"], list_ret["comment"]
    for data in list_ret["ret"]:
        if resource.resource_id == data["resource_id"]:
            break
    else:
        assert False, "Resource_id not found in list"
